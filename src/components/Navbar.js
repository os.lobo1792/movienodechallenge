import React from "react";

const Navbar = () => {
  return (
    <nav className="navbar navbar-dark bg-dark border-bottom border-white">
      <div className="container">
        <a className="navbar-brand" href="/">
        MovieNode Challenge!
        </a>
      </div>
    </nav>
  );
};

export default Navbar;
